package ru.ivanova.task002;

import ru.ivanova.task002.ru.ivanova.task002_const.Terminal_const;

public class App {

    public static void main( final String[] args) {
	System.out.println("** Welcome to task_002_project");
	parseArgs(args);
    }

    private static void parseArgs (final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        if (Terminal_const.HELP.equals(arg))showHelp();
        if (Terminal_const.ABOUT.equals(arg))showAbout();
        if (Terminal_const.VERSION.equals(arg))showVersion();

    }

        private static void showHelp(){
            System.out.println("[HELP]");
            System.out.println(Terminal_const.ABOUT + "- Show developer info.");
            System.out.println(Terminal_const.VERSION +"- Show version info.");
            System.out.println(Terminal_const.HELP +"- Display terminal commands.");

        }

        private static void showAbout() {
            System.out.println("[ABOUT]");
            System.out.println("NAME: Ivanova Anna");
            System.out.println("EMAIL: cat1438@mail.ru");
        }

        private static void showVersion() {
            System.out.println("[VERSION]");
            System.out.println("1.0.0");
        }

    }


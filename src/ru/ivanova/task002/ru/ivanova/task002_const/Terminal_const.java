package ru.ivanova.task002.ru.ivanova.task002_const;

public class Terminal_const {
    public static final String HELP  = "help";
    public static final String VERSION  = "version";
    public static final String ABOUT  = "about";
}
